#include <wups.h>
#include <malloc.h>
#include <cstdio>
#include <string.h>
#include <nsysnet/socket.h>
#include <utils/logger.h>
#include <fs/FSUtils.h>

#include <coreinit/filesystem.h>
#include <coreinit/cache.h>
#include <coreinit/memorymap.h>
#include <coreinit/title.h>

// (barely) modified from https://github.com/Maschell/DiiBuggerWUPS
void WriteCode(uint32_t address, uint32_t instr)
{
    address += 0x0A000000;
    uint32_t replace_instr = instr;

    ICInvalidateRange(&replace_instr, 4);
    DCFlushRange(&replace_instr, 4);

    uint32_t codeSize = 4;
    WUPS_KernelCopyDataFunction(
        (uint32_t)OSEffectiveToPhysical(address),
        (uint32_t)OSEffectiveToPhysical((uint32_t)&replace_instr),
        codeSize
    );

    ICInvalidateRange((void*)address, codeSize);
    DCFlushRange((void*)address, codeSize);
}

struct MODSHeader
{
    uint32_t magic; // "MODS"
    uint32_t version;
    uint32_t numCodes;
    uint32_t pad;
};

struct MODSFile
{
    MODSHeader header;

    uint32_t data;
};

void applyModsFile (MODSFile *file)
{
    uint32_t *code = &file->data;

    for (uint32_t i = 0; i < file->header.numCodes; ++i) {
        uint32_t codeType = *(code++);
        uint32_t addr = *(code++);
        uint32_t numInstrs = *(code++);

        for (uint32_t j = 0; j < numInstrs; ++j) {
            WriteCode(addr, *(code++));
            addr += 4;
        }
    }
}

void loadModsFile (const char *filename)
{    
    struct stat filestat;
    if (stat(filename, &filestat))
        return;

    log_printf("[bfh] Loading codes from %s\n...", filename);

    MODSFile *file = (MODSFile*)malloc(filestat.st_size);
    FSUtils::LoadFileToMem(filename, (uint8_t**)&file, NULL);

    applyModsFile(file);

    free(file);
}

WUPS_PLUGIN_NAME("RPX Patcher");
WUPS_PLUGIN_DESCRIPTION("Apples ssssssssssssssssss");
WUPS_PLUGIN_VERSION("v1.0");
WUPS_PLUGIN_AUTHOR("mOatles");
WUPS_PLUGIN_LICENSE("GPL3");

WUPS_FS_ACCESS()
WUPS_ALLOW_KERNEL()

ON_APPLICATION_START(){
    socket_lib_init();
    log_init();

    char filename[32] = {0};
    uint64_t titleId = OSGetTitleID();
    sprintf(filename, "sd:/%016llX.mods", titleId);

    loadModsFile(filename);
}
